# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from bbbbutton.models import PersistentMeeting
from django.contrib.auth.decorators import login_required


@login_required
def button(request, persistent_meeting_id):
    persistent_meeting = PersistentMeeting.objects.get(id=persistent_meeting_id)
    return render(
        request,
        "bbbbutton/button.html",
        {
            "name": persistent_meeting.name,
            "num_active_users": persistent_meeting.get_num_active_users(),
            "id": persistent_meeting.id,
            "enabled": persistent_meeting.enabled,
        },
    )


# @login_required
def join(request, persistent_meeting_id):
    persistent_meeting = get_object_or_404(PersistentMeeting, id=persistent_meeting_id)
    user = request.user if request.user.is_authenticated else None
    return persistent_meeting.join(user, request)
