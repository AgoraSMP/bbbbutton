# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django import template
from bbbbutton.models import PersistentMeeting

register = template.Library()


@register.inclusion_tag("bbbbutton/button.html")
def bbbbutton(persistent_meeting, user):
    assert isinstance(
        persistent_meeting, PersistentMeeting
    ), "The object passed to {% bbbbutton THIS_THING_HERE %} does not exist in context or is not an instance of PersistentMeeting."
    allowed = persistent_meeting.user_may_enter(user)

    return {
        "name": persistent_meeting.name,
        "num_active_users": persistent_meeting.get_num_active_users(),
        "id": persistent_meeting.id,
        "enabled": persistent_meeting.enabled,
        "allowed": allowed,
    }
