# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
import secrets
import socket
import string
from datetime import timedelta

from requests import Timeout
import requests.packages.urllib3.util.connection as urllib3_cn
from bigbluebutton import BigBlueButton
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db import models
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.utils import timezone
from django.urls import reverse

# TODO we might want to reconsider the coupling between usermgr and bbbbutton
# since right now both cross-reference each other -- which usually means they
# should not be two different apps

# from usermgr.models import Seminar, Workgroup


logger = logging.getLogger(__name__)


# HACK to disable IPv6 on BBB API
def allowed_gai_family():
    return socket.AF_INET


urllib3_cn.allowed_gai_family = allowed_gai_family


def random_password():
    return "".join(secrets.choice(string.ascii_letters) for _ in range(30))


class PersistentMeetingManager(models.Manager):
    def for_seminar_view(self, user: User, seminar, workgroups):
        return (
            self.filter(
                Q(enabled=True)  # only those that are generally enabled
                | Q(
                    privileged_users=user
                ),  # and those that this user has special permissions for
                (
                    Q(seminar__isnull=True) & Q(workgroup__isnull=True)
                )  # meeting is global
                | Q(seminar=seminar)  # meeting is seminar-bound
                | Q(workgroup__in=workgroups),  # meeting is workgroup-bound
            )
            .order_by("name")
            .distinct()
        )


class PersistentMeeting(models.Model):
    name = models.CharField(max_length=100, unique=True)
    attendee_pw = models.CharField(max_length=30, default=random_password)
    moderator_pw = models.CharField(max_length=30, default=random_password)
    privileged_are_moderators = models.BooleanField(
        default=True,
        help_text="Privileged users will be able to take the presenter, start a screenshare, etc.",
    )
    registered_are_moderators = models.BooleanField(
        default=True,
        help_text="All users will be able to take the presenter, start a screenshare, etc.",
    )
    enabled = models.BooleanField(default=False)
    num_users_cached = models.IntegerField(default=0)
    num_users_cached_timeout = models.DateTimeField(auto_now_add=True, editable=True)
    # num_users_cached_timeout.editable=True # uncomment for debugging the cache in django admin
    seminar = models.ForeignKey(
        "usermgr.Seminar", on_delete=models.CASCADE, blank=True, null=True
    )
    workgroup = models.ForeignKey(
        "usermgr.Workgroup", on_delete=models.CASCADE, blank=True, null=True
    )
    privileged_users = models.ManyToManyField(
        User,
        blank=True,
        help_text="Privileged users can join a meeting even if it is not enabled. Use this to allow your moderators and speakers to enter and get ready before you let your audience join.",
    )
    public = models.BooleanField(
        default=False,
        help_text="Disables all protections and lets everyone with the join link enter the meeting without an account.",
    )

    objects = PersistentMeetingManager()

    _api = BigBlueButton(settings.BBB_API_URL, settings.BBB_API_SECRET)

    def __str__(self):
        return self.name

    def meeting_id(self):
        return "persistent-meeting-{}".format(self.id)

    def create(self):
        create_time = self._api.create_meeting(
            meeting_id=self.meeting_id(),
            meeting_name=self.name,
            attendee_password=self.attendee_pw,
            moderator_password=self.moderator_pw,
            logout_url=reverse("index"),
            record=False,
            allow_start_stop_recording=False,
        )
        if not create_time:
            raise RuntimeError("Failed to create meeting")
        return create_time

    def user_may_enter(self, user):
        if self.public == True and self.enabled == True:
            return True
        if user is None:
            return False
        if self.privileged_users.filter(pk=user.pk).exists():
            return True
        if not self.enabled:
            return False
        if self.workgroup is not None:
            if not self.workgroup.seminar.is_access_open():
                return False
            return user.profile.workgroups.filter(pk=self.workgroup.pk).exists()
        if self.seminar is not None:
            if not self.seminar.is_access_open():
                return False
            return user.profile.workgroups.filter(seminar=self.seminar).exists()
        return True

    def user_can_moderate(self, user: User):
        # guests can never join as moderators
        if user is None:
            return False
        # superusers are always moderators
        if user.is_superuser:
            return True

        if self.registered_are_moderators:
            return True
        if (
            self.privileged_are_moderators
            and self.privileged_users.filter(pk=user.pk).exists()
        ):
            return True

        # workgroup- or seminar-bound meetings inherit moderator permissions
        # from the workgroup/seminar
        if self.seminar is not None:
            # user needs to be seminar admin
            return user.has_perm("change_seminar", self.seminar)
        if self.workgroup is not None:
            # user needs to be workgroup moderator or seminar admin of that workgroup's seminar
            return user.has_perm("change_workgroup", self.workgroup) or user.has_perm(
                "change_seminar", self.workgroup.seminar
            )
        # global meetings don't inherit moderatorship
        return False

    def join(self, user, request):
        if not self.user_may_enter(user):
            if not self.enabled:
                # raise PermissionDenied("Meeting \"{}\" is currently closed".format(self.name))
                return render(request, "bbbbutton/wait.html")
            if user is None:
                return redirect("login")
            raise PermissionDenied(
                'User "{}" has no permission to enter meeting "{}".'.format(
                    user.username, self.name
                )
            )
        create_time = self.create()
        if self.user_can_moderate(user):
            pw = self.moderator_pw
        else:
            pw = self.attendee_pw
        username = user.get_full_name() if user is not None else "Guest"
        join_url = self._api.join_meeting_url(
            meeting_id=self.meeting_id(),
            name=username,
            password=pw,
            options={"createTime": create_time},
        )
        return HttpResponseRedirect(join_url)

    def get_num_active_users(self):
        if not self.enabled:
            return 0
        now = timezone.now()
        if now < self.num_users_cached_timeout:
            return self.num_users_cached
        try:
            meeting_id = self.meeting_id()
            info = self._api.meeting_info(meeting_id=meeting_id)
            if info is None:
                logger.warn(
                    "Failed to retrieve meeting info for %s from BigBlueButton backend",
                    meeting_id,
                )
                num_users = 0
            else:
                num_users = len(info["users"])
        except Timeout:
            logger.warn(
                "Timeout while retrieving meeting info for %s from BigBlueButton backend",
                meeting_id,
            )
            num_users = 0
        except:
            # this is a minor feature and if something goes terribly wrong, we'd rather keep
            # the user counter at zero than break the entire frontend with a 500 error
            logger.exception(
                "Unspecified error calling BigBlueButton backend, suppressing"
            )
            num_users = 0
        self.num_users_cached_timeout = now + timedelta(seconds=30)
        self.num_users_cached = num_users
        self.save(update_fields=["num_users_cached_timeout", "num_users_cached"])
        return num_users

    def set_status(self, enabled):
        self.enabled = enabled
        self.save()
