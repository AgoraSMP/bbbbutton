# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.contrib import admin

from .models import PersistentMeeting
from usermgr.admin_base import SeminarBoundModelMixin, UsingSeminarBoundFieldMixin


@admin.register(PersistentMeeting)
class PersistentMeetingAdmin(
    UsingSeminarBoundFieldMixin, SeminarBoundModelMixin, admin.ModelAdmin
):
    list_display = ["name", "seminar", "workgroup", "enabled"]
    list_filter = ["enabled", "seminar", "workgroup"]
    filter_horizontal = ["privileged_users"]
    search_fields = ["name"]

    fieldsets = (
        (None, {"fields": ("name", "enabled", "seminar", "workgroup")}),
        (
            "Permissions",
            {
                "fields": (
                    "privileged_are_moderators",
                    "registered_are_moderators",
                    "public",
                    "privileged_users",
                )
            },
        ),
        ("Advanced", {"fields": ("attendee_pw", "moderator_pw")}),
    )
