# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.urls import path, include

from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path("join/<int:persistent_meeting_id>", views.join, name="bbbbutton_join"),
    path("button/<int:persistent_meeting_id>", views.button, name="bbbbutton_button"),
]
